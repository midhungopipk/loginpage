import { LOGIN, LOGOUT } from "./authAction";

initialState = { userId: null, token: null };

export default (state = initialState, action) => {
	switch (action.type) {
		case LOGIN:
			return {
				userId: action.userId,
				token: action.token,
			};
		case LOGOUT:
			return { userId: null, token: null };
		default:
			return state;
	}
};
