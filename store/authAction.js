export const LOGIN = "LOGIN";
export const LOGOUT = "LOGOUT";

export const login = (email, password) => {
	return async (dispatch) => {
		const response = await fetch("http://cm.wemakeiot.net:3200/login", {
			method: "POST",
			headers: { "Content-Type": "application/json" },
			body: JSON.stringify({
				username: email,
				password: password,
			}),
		});
		if(!response.ok){
			throw new Error(message);
		}
		const resData = await response.json();
		dispatch({
			type: LOGIN,
			userId: resData._id,
			token: resData.token,
		});
	};
};

export const logout = () => {
	return { type: LOGOUT };
};
