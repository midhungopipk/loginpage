import React, { useState } from "react";
import { View, Text,StyleSheet } from "react-native";
import { Formik } from "formik";
import { TextInput, Card, Title, Button, IconButton } from "react-native-paper";
import * as yup from "yup";

const SignUpScreen = (props) => {
	const [secureTextEntry, setSecureTextEntry] = useState(true);
	const [secureTextEntry2, setSecureTextEntry2] = useState(true);

	const [isSignUp, setIsSignUp] = useState(false);

	const signUpValidationSchema = yup.object().shape({
		email: yup.string().email("Plese enter a valid email").required("Email Address is required"),
		password: yup
			.string()
			.min(8, ({ min }) => `Password must be ${min} characters`)
			.required("Password is required"),
		confirmPassword: yup
			.string()
			.oneOf([yup.ref("password")], "Password do not match")
			.required("Confirm password is required"),
	});
	const { navigation } = props;

	return (
		<View style={styles.container}>
			<Card style={styles.card}>
				<Formik
					validationSchema={signUpValidationSchema}
					initialValues={{
						email: "",
						password: "",
						confirmPassword: "",
					}}
					onSubmit={(values) => {
						navigation.navigate("Home"), console.log(values);
					}}
				>
					{({ handleChange, handleBlur, handleSubmit, values, isValid, errors, touched }) => (
						<Card.Content>
							<Title style={styles.title}>{isSignUp ? "SignUp" : "Login"}</Title>
							<TextInput
								letterSpacing={3}
								name="email"
								mode="flat"
								underlineColor="black"
								placeholder="E-mail"
								style={styles.inputContainer}
								onChangeText={handleChange("email")}
								onBlur={handleBlur("email")}
								value={values.email}
								keyboardType="email-address"
							/>
							{errors.email && touched.email && <Text style={{ fontSize: 10, color: "red" }}>{errors.email}</Text>}
							<TextInput
								letterSpacing={3}
								name="password"
								mode="flat"
								underlineColor="black"
								placeholder="Password"
								style={styles.inputContainer}
								onChangeText={handleChange("password")}
								onBlur={handleBlur("password")}
								value={values.password}
								secureTextEntry={secureTextEntry}
								right={
									<TextInput.Icon
										name={secureTextEntry ? "eye-off" : "eye"}
										onPress={() => {
											setSecureTextEntry(!secureTextEntry);
										}}
									/>
								}
							/>
							{errors.password && touched.password && <Text style={{ fontSize: 10, color: "red" }}>{errors.password}</Text>}

							<TextInput
								letterSpacing={3}
								name="confirmPassword"
								mode="flat"
								underlineColor="black"
								placeholder="Confirm Password"
								style={styles.inputContainer}
								onChangeText={handleChange("confirmPassword")}
								onBlur={handleBlur("confirmPassword")}
								value={values.confirmPassword}
								secureTextEntry={secureTextEntry2}
								right={
									<TextInput.Icon
										name={secureTextEntry2 ? "eye-off" : "eye"}
										onPress={() => {
											setSecureTextEntry2(!secureTextEntry2);
										}}
									/>
								}
							/>
							{errors.confirmPassword && touched.confirmPassword && <Text style={{ fontSize: 10, color: "red" }}>{errors.confirmPassword}</Text>}

							<View style={styles.button}>
								<Button onPress={handleSubmit} mode="contained" color="red" style={{ width: 200, marginBottom: 10 }} disabled={!isValid}>
									{isSignUp ? "SignUp" : "Login"}
								</Button>

								<IconButton
									icon="account-convert-outline"
									onPress={() => {
										setIsSignUp(!isSignUp);
									}}
									size={29}
									color="red"
								/>
							</View>
						</Card.Content>
					)}
				</Formik>
			</Card>
		</View>
	);
};

const styles = StyleSheet.create({
	title: {
		fontSize: 25,
		textAlign: "center",
		marginBottom: 15,
	},
	card: {
		minHeight: 300,
		width: "90%",
		elevation: 8,
	},
	container: {
		alignItems: "center",
		justifyContent: "center",
		flex: 1,
	},
	inputContainer: {
		marginBottom: 20,
	},
	button: {
		justifyContent: "center",
		alignItems: "center",
	},
});

export default SignUpScreen;
