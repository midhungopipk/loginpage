import React from "react";
import { View, StyleSheet, ScrollView, TouchableWithoutFeedback, Keyboard, Alert } from "react-native";
import { Card, Avatar, TextInput, Button } from "react-native-paper";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import { useDispatch } from "react-redux";
import * as authActions from "../store/authAction";

const ProfileScreen = (props) => {
	const dispatch = useDispatch();
	return (
		<TouchableWithoutFeedback onPress={Keyboard.dismiss}>
			<View style={styles.screen}>
				<View
					style={{
						backgroundColor: "darkcyan",
						height: 150,
						justifyContent: "center",
						alignItems: "center",
						borderRadius: 25,
					}}
				>
					<Avatar.Image
						source={{
							uri: "https://st.depositphotos.com/2101611/3925/v/600/depositphotos_39258143-stock-illustration-businessman-avatar-profile-picture.jpg",
						}}
						size={150}
						style={{
							marginTop: 150,
							elevation: 20,
						}}
					/>
				</View>

				<View
					style={{
						alignItems: "center",
						justifyContent: "center",
						flex: 1,
					}}
				>
					<Card
						style={{
							backgroundColor: "white",
							width: "90%",
							height: 500,
							borderRadius: 30,
							elevation: 20,
						}}
					>
						<ScrollView
							contentContainerStyle={{
								margin: 5,
							}}
						>
							<View style={styles.textInputContainer}>
								<MaterialCommunityIcons name="account" color="darkcyan" size={30} style={styles.icon} />
								<TextInput letterSpacing={3} value="FULL NAME" style={styles.textInput} editable={false} />
							</View>
							<View style={styles.textInputContainer}>
								<MaterialCommunityIcons name="phone" color="darkcyan" size={30} style={styles.icon} />
								<TextInput letterSpacing={3} value="987654321" keyboardType="phone-pad" style={styles.textInput} editable={false} />
							</View>
							<View style={styles.textInputContainer}>
								<MaterialCommunityIcons name="calendar-range" color="darkcyan" size={30} style={styles.icon} />

								<TextInput value="1/01/2022" letterSpacing={3} style={styles.textInput}  editable={false} />
							</View>
							<View style={styles.textInputContainer}>
								<MaterialCommunityIcons name="map-marker" color="darkcyan" size={30} style={styles.icon} />
								<TextInput letterSpacing={3} value="Home Address" style={styles.textInput} editable={false} />
							</View>
							<View style={styles.textInputContainer}>
								<MaterialCommunityIcons name="email" color="darkcyan" size={30} style={styles.icon} />
								<TextInput letterSpacing={3} value="webqatesting@gmail.com" keyboardType="email-address" style={styles.textInput} editable={false} />
							</View>

							<View
								style={{
									justifyContent: "center",
									alignItems: "center",
									flex: 1,
								}}
							>
								<Button
									icon="lock"
									mode="contained"
									color="darkcyan"
									contentStyle={{
										height: 60,
									}}
									style={{
										marginTop: 40,
										width: "90%",
										borderRadius: 25,
									}}
									onPress={() => console.log("change password pressed")}
								>
									change password
								</Button>
								<Button
									icon="logout"
									mode="text"
									color="darkcyan"
									contentStyle={{
										height: 60,
									}}
									style={{
										marginTop: 30,
										width: "90%",
										borderRadius: 25,
									}}
									onPress={() => {
										Alert.alert("Loging out", "Are you sure?", [
											{
												text: "No",
												style: "cancel",
											},
											{
												text: "Yes",
												onPress: () => dispatch(authActions.logout()),
											},
										]);
									}}
								>
									Logout
								</Button>
							</View>
						</ScrollView>
					</Card>
				</View>
			</View>
		</TouchableWithoutFeedback>
	);
};

const styles = StyleSheet.create({
	screen: {
		flex: 1,
	},
	textInput: {
		backgroundColor: "white",
		width: "90%",
		height: 40,
		flex: 1,
		padding: 10,
	},
	textInputContainer: {
		flex: 1,
		flexDirection: "row",
		justifyContent: "center",
		alignItems: "center",
	},
	icon: { padding: 10 },
});

export default ProfileScreen;
