import React, { useState } from "react";
import { View, Text, StyleSheet, ScrollView } from "react-native";
import { AppBar, Backdrop, BackdropSubheader } from "@react-native-material/core";
import { Button, IconButton } from "react-native-paper";
import { useSafeAreaInsets } from "react-native-safe-area-context";
import { SectionList } from "react-native";

const HomeScreen = (props) => {
	const [revealed, setRevealed] = useState(false);
	const insets = useSafeAreaInsets();
	const DATA = [
		{
			title: "Main dishes",
			data: ["Pizza", "Burger", "Risotto"],
		},
		{
			title: "Sides",
			data: ["French Fries", "Onion Rings", "Fried Shrimps"],
		},
		{
			title: "Drinks",
			data: ["Water", "Coke", "Beer"],
		},
		{
			title: "Desserts",
			data: ["Cheese Cake", "Ice Cream"],
		},
		{
			title: "Main dishes",
			data: ["Pizza", "Burger", "Risotto"],
		},
		{
			title: "Sides",
			data: ["French Fries", "Onion Rings", "Fried Shrimps"],
		},
		{
			title: "Drinks",
			data: ["Water", "Coke", "Beer"],
		},
		{
			title: "Desserts",
			data: ["Cheese Cake", "Ice Cream"],
		},
	];
	return (
		<View style={{ flex: 1 }}>
			<Backdrop
				revealed={revealed}
				style={{ backgroundColor: "darkcyan" }}
				backLayer={
					<View style={{ height: 300, padding: 20 }}>
						<ScrollView showsVerticalScrollIndicator={false}>
							<Button
								icon="arrow-right-thick"
								mode="contained"
								color="white"
								contentStyle={{
									height: 50,
									justifyContent: "flex-start",
								}}
								labelStyle={{ fontSize: 20 }}
								style={styles.backlayerButton}
								onPress={() => console.log("Pressed")}
							>
								Select this data to render
							</Button>
							<Button
								icon="arrow-right-thick"
								mode="contained"
								color="white"
								contentStyle={{
									height: 50,
									justifyContent: "flex-start",
								}}
								labelStyle={{ fontSize: 20 }}
								style={styles.backlayerButton}
								onPress={() => console.log("Pressed")}
							>
								Select this data to render
							</Button>
							<Button
								icon="arrow-right-thick"
								mode="contained"
								color="white"
								contentStyle={{
									height: 50,
									justifyContent: "flex-start",
								}}
								labelStyle={{ fontSize: 20 }}
								style={styles.backlayerButton}
								onPress={() => console.log("Pressed")}
							>
								Select this data to render
							</Button>
							<Button
								icon="arrow-right-thick"
								mode="contained"
								color="white"
								contentStyle={{
									height: 50,
									justifyContent: "flex-start",
								}}
								labelStyle={{ fontSize: 20 }}
								style={styles.backlayerButton}
								onPress={() => console.log("Pressed")}
							>
								Select this data to render
							</Button>
							<Button
								icon="arrow-right-thick"
								mode="contained"
								color="white"
								contentStyle={{
									height: 50,
									justifyContent: "flex-start",
								}}
								labelStyle={{ fontSize: 20 }}
								style={styles.backlayerButton}
								onPress={() => console.log("Pressed")}
							>
								Select this data to render
							</Button>
							<Button
								icon="arrow-right-thick"
								mode="contained"
								color="white"
								contentStyle={{
									height: 50,
									justifyContent: "flex-start",
								}}
								labelStyle={{ fontSize: 20 }}
								style={styles.backlayerButton}
								onPress={() => console.log("Pressed")}
							>
								Select this data to render
							</Button>
							<Button
								icon="arrow-right-thick"
								mode="contained"
								color="white"
								contentStyle={{
									height: 50,
									justifyContent: "flex-start",
								}}
								labelStyle={{ fontSize: 20 }}
								style={styles.backlayerButton}
								onPress={() => console.log("Pressed")}
							>
								Select this data to render
							</Button>
							<Button
								icon="arrow-right-thick"
								mode="contained"
								color="white"
								contentStyle={{
									height: 50,
									justifyContent: "flex-start",
								}}
								labelStyle={{ fontSize: 20 }}
								style={styles.backlayerButton}
								onPress={() => console.log("Pressed")}
							>
								Select this data to render
							</Button>
						</ScrollView>
					</View>
				}
				frontLayerContainerStyle={{
					borderTopEndRadius: 40,
					borderTopStartRadius: 40,
				}}
				subheaderContainerStyle={{ backgroundColor: "black" }}
				header={
					<AppBar
						title="Home"
						titleStyle={{ fontWeight: "bold", fontSize: 25 }}
						transparent
						style={{ paddingTop: insets.top }}
						leading={
							<IconButton
								icon={revealed ? "close" : "menu"}
								color="white"
								size={30}
								onPress={() => {
									setRevealed(!revealed);
								}}
							/>
						}
					/>
				}
			>
				<BackdropSubheader
					title={
						<View>
							<Text style={{ fontWeight: "bold", fontSize: 24 }}>Section List</Text>
						</View>
					}
					trailing={
						<IconButton
							icon={revealed ? "arrow-up-thick" : "arrow-down-thick"}
							onPress={() => {
								setRevealed(!revealed);
							}}
						/>
					}
				/>
				<View style={{paddingBottom:150}}>
					<SectionList
						keyExtractor={(item, index) => index.toString()}
						sections={DATA}
						renderItem={(itemData) => (
							<View style={styles.listItemData}>
								<Text style={{ fontSize: 18 }}>{itemData.item}</Text>
							</View>
						)}
						showsVerticalScrollIndicator={false}
						renderSectionHeader={({ section }) => (
							<View style={styles.listItem}>
								<Text
									style={{
										fontSize: 22,
										fontWeight: "bold",
										color: "white",
									}}
								>
									{section.title}
								</Text>
							</View>
						)}
					/>
				</View>
			</Backdrop>
		</View>
	);
};

const styles = StyleSheet.create({
	screen: {
		flex: 1,
		justifyContent: "center",
		alignItems: "center",
	},
	listItem: {
		marginVertical: 15,
		height: 50,
		margin: 10,
		backgroundColor: "darkcyan",
		alignItems: "center",
		justifyContent: "center",
		borderRadius: 25,
		elevation: 6,
	},
	listItemData: {
		marginVertical: 15,
		alignItems: "center",
		justifyContent: "center",
	},
	backlayerButton: { borderRadius: 25, marginVertical: 10 },
});

export default HomeScreen;
