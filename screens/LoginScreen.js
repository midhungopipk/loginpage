import React, { useState } from "react";
import { View, Text, KeyboardAvoidingView, StyleSheet, ImageBackground, Alert, ScrollView, Dimensions, TouchableWithoutFeedback, Keyboard } from "react-native";
import { Formik } from "formik";
import { TextInput, Button, Avatar } from "react-native-paper";
import * as yup from "yup";
import { SocialIcon } from "react-native-elements";
import { useDispatch } from "react-redux";
import * as authActions from "../store/authAction";
import { ActivityIndicator } from "@react-native-material/core";
import { useSafeAreaInsets } from "react-native-safe-area-context";

const LoginScreen = (props) => {
	const [secureTextEntry, setSecureTextEntry] = useState(true);
	const [isSignUp, setIsSignUp] = useState(false);
	const [isAuthenticating, setIsAuthenticating] = useState(false);
	const insets = useSafeAreaInsets();

	const dispatch = useDispatch();
	const loginValidationSchema = yup.object().shape({
		email: yup.string().email("Plese enter a valid email").required("Email Address is required"),
		password: yup
			.string()
			.min(8, ({ min }) => `Password must be ${min} characters`)
			.required("Password is required"),
	});
	const { navigation } = props;

	return (
		<ImageBackground
			style={styles.backgroundImage}
			blurRadius={4}
			source={{
				uri: "https://cutewallpaper.org/23/abstract-wallpaper-cyan/2678811385.jpg",
			}}
		>
			<TouchableWithoutFeedback style={{ flex: 1 }} onPress={Keyboard.dismiss}>
				<KeyboardAvoidingView behavior="padding" keyboardVerticalOffset={100}>
					<View style={{ alignItems: "center", justifyContent: "center", marginTop: insets.top }}>
						<Avatar.Image
							source={{
								uri: "https://st2.depositphotos.com/1531183/5770/v/600/depositphotos_57709697-stock-illustration-male-person-silhouette-profile-picture.jpg",
							}}
							size={100}
							style={styles.logo}
						/>

						<ScrollView style={styles.form} showsVerticalScrollIndicator={false}>
							<Formik
								validationSchema={loginValidationSchema}
								initialValues={{ email: "", password: "" }}
								onSubmit={async (values) => {
									try {
										setIsAuthenticating(true);
										await dispatch(authActions.login(values.email, values.password));
									} catch (error) {
										Alert.alert("Login failed", "Please check your E-mail and Password", [
											{
												text: "Okay",
												style: "destructive",
											},
										]);
										setIsAuthenticating(false);
									}
								}}
							>
								{({ handleChange, handleBlur, handleSubmit, values, isValid, errors, touched }) => (
									<View>
										<Text style={styles.title}>{isSignUp ? "SignUp" : "Login"}</Text>
										<View style={styles.socialIconContainer}>
											<SocialIcon type="google" light onPress={() => console.log("google")} />
											<SocialIcon type="twitter" light onPress={() => console.log("twitter")} />
											<SocialIcon type="facebook" light onPress={() => console.log("facebook")} />
											<SocialIcon type="linkedin" light onPress={() => console.log("linkedin")} />
										</View>

										<View style={styles.divider} />

										{errors.email && touched.email && <Text style={styles.errorText}>*{errors.email}</Text>}

										<TextInput
											letterSpacing={3}
											name="email"
											mode="flat"
											underlineColor="#ccc"
											placeholder="E-mail"
											style={styles.inputBox}
											onChangeText={handleChange("email")}
											onBlur={handleBlur("email")}
											value={values.email}
											keyboardType="email-address"
											activeUnderlineColor="black"
										/>
										{errors.password && touched.password && <Text style={styles.errorText}>*{errors.password}</Text>}
										<TextInput
											letterSpacing={3}
											name="password"
											mode="flat"
											underlineColor="#ccc"
											placeholder="Password"
											style={styles.inputBox}
											onChangeText={handleChange("password")}
											onBlur={handleBlur("password")}
											value={values.password}
											secureTextEntry={secureTextEntry}
											right={
												<TextInput.Icon
													name={secureTextEntry ? "eye-off" : "eye"}
													onPress={() => {
														setSecureTextEntry(!secureTextEntry);
													}}
												/>
											}
											activeUnderlineColor="black"
										/>
										<View style={styles.forgotPasswordTextContainer}>
											<Text style={styles.forgotPasswordText}>Did you forgot your Password?</Text>

											<Button icon="lock-reset" mode="contained" color="black" disabled={isAuthenticating} compact onPress={() => console.log("Pressed")}>
												Reset
											</Button>
										</View>
										<View style={styles.buttonContainer}>
											{isAuthenticating ? (
												<ActivityIndicator size={"large"} color="darkcyan" />
											) : (
												<Button onPress={handleSubmit} mode="contained" color="white" style={styles.button} disabled={!isValid}>
													{isSignUp ? "SignUp" : "Login"}
												</Button>
											)}
											<Text style={{ color: "white" }}>or</Text>
											<Button
												icon="account-convert-outline"
												onPress={() => {
													setIsSignUp(!isSignUp);
												}}
												mode="contained"
												color="darkcyan"
												style={styles.button}
											>
												Switch to {isSignUp ? "login" : "Signup"}
											</Button>
										</View>
									</View>
								)}
							</Formik>
						</ScrollView>
					</View>
				</KeyboardAvoidingView>
			</TouchableWithoutFeedback>
		</ImageBackground>
	);
};

const styles = StyleSheet.create({
	title: {
		fontSize: 25,
		textAlign: "center",
		marginBottom: 15,
		marginTop: 15,
		color: "white",
	},
	form: {
		minHeight: 300,
		width: "90%",
		paddingBottom: 50,
	},
	inputBox: {
		marginBottom: 20,
		borderRadius: 20,
		borderTopEndRadius: 20,
		borderTopStartRadius: 20,
		overflow: "hidden",
		backgroundColor: "white",
		height: 50,
	},
	buttonContainer: {
		justifyContent: "center",
		alignItems: "center",
		borderRadius: 25,
	},
	button: {
		width: 200,
		margin: 10,
		borderRadius: 25,
	},
	logo: {
		margin: 10,
		elevation: 6,
	},
	forgotPasswordText: {
		color: "white",
		marginTop: 10,
	},
	divider: {
		borderBottomColor: "black",
		borderBottomWidth: 1,
		marginBottom: 25,
		elevation: 3,
	},
	backgroundImage: {
		width: Dimensions.get("screen").width,
		height: Dimensions.get("screen").height,
	},
	socialIconContainer: {
		flexDirection: "row",
		justifyContent: "space-between",
		marginBottom: 5,
	},
	errorText: {
		fontSize: 14,
		color: "red",
		fontWeight: "bold",
	},
	forgotPasswordTextContainer: {
		flexDirection: "row",
		justifyContent: "space-between",
		marginBottom: 20,
	},
});

export default LoginScreen;
