import React from "react";
import { StyleSheet } from "react-native";

import { combineReducers, createStore, applyMiddleware } from "redux";
import { Provider } from "react-redux";
import authReducer from "./store/authReducer";
import ReduxThunk from "redux-thunk";
import AppNavigator from "./navigator/AppNavigator";

export default function App() {
	const rootReducer = combineReducers({
		auth: authReducer,
	});
	const store = createStore(rootReducer, applyMiddleware(ReduxThunk));

	return (
		<Provider store={store}>
			<AppNavigator />
		</Provider>
	);
}

const styles = StyleSheet.create({});
