import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { AuthNavigator } from "./AuthStackNavigator";
import { BottomNavigator } from "./AppBottomNavigator";
import { useSelector } from "react-redux";

const AppNavigator = (prop) => {
	const isAuth = useSelector((state) => state.auth.token);

	return (
		<NavigationContainer>
			{!isAuth && <AuthNavigator />}
			{isAuth && <BottomNavigator />}
		</NavigationContainer>
	);
};

export default AppNavigator;
