import React from "react";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import { HomeNavigator } from "./HomeStackNavigator";
import { ProfileNavigator } from "./ProfileStackNavigator";

const AppBottomNavigator = createBottomTabNavigator();
export const BottomNavigator = () => {
	return (
		<AppBottomNavigator.Navigator
			screenOptions={{
				tabBarActiveTintColor: "darkcyan",
				tabBarShowLabel: false,
				tabBarStyle: {
					elevation: 3,
					height: 60,
					borderRadius: 18,
					marginBottom: 10,
					marginHorizontal: 10,
					position: "absolute",
				},
			}}
		>
			<AppBottomNavigator.Screen
				name="HomeTab"
				component={HomeNavigator}
				options={{
					tabBarIcon: ({ color }) => <MaterialCommunityIcons name="home" color={color} size={40} />,
					headerShown: false,
				}}
			/>
			<AppBottomNavigator.Screen
				name="ProfileTab"
				component={ProfileNavigator}
				options={{
					tabBarIcon: ({ color }) => <MaterialCommunityIcons name="account" color={color} size={40} />,
					headerShown: false,
				}}
			/>
		</AppBottomNavigator.Navigator>
	);
};
