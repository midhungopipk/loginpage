import React from "react";
import { createStackNavigator } from "@react-navigation/stack";

import LoginScreen from "../screens/LoginScreen";
const AuthStackNavigator = createStackNavigator();
export const AuthNavigator = () => {
	return (
		<AuthStackNavigator.Navigator>
			<AuthStackNavigator.Screen name="Auth" component={LoginScreen} options={{ headerShown: false }} />
		</AuthStackNavigator.Navigator>
	);
};
